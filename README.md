# 使用手册

## change log
| tag version | change logs | date |
| --- | --- | --- |
| v7.0.0 | 1.新增gateway对令牌有效期的判断<br/>2.修复上一个版本中refresh_token换取新token后端报错问题<br/>3.新增business-server模块，前后端分离实现用户登录、退出、更换token操作 |2020-01-29|
| v6.0.0 | 1.扩展用户信息到jwt令牌<br/>2.资源服务上下文保存和取出用户信息 | 2021-01-14 |
| v5.0.0 | 1.新增eureka注册中心服务register-server<br/>2.新增网关服务gateway-server<br/>3.修改资源服务，移除OAuth2.0以及JWT依赖和相关配置<br/>4.实现初版分布式认证和鉴权功能 |2020-01-13|
| v4.0.0 | 1.使用jwt令牌功能<br/>2.客户端信息保存到数据库，用户授权码保存到数据库</br>3.新增common模块保存公共功能| 2020-01-11 |
| v3.0.0 | 1. 新增资源服务，可进行接口测试<br/>2. 修改了auth-center/docs/sql/init.sql | 2020-01-10 |
| v2.0.0 | 实现自定义登陆页面、自定义授权页面 | 2021-01-07|
| v1.0.0 | 实现初版认证服务器，可进行接口测试<br/>遗留问题：<br/>1. 登录页面加载速度太慢<br/>2. 授权页面太丑 | 2021-01-07 |

## 文章介绍

> [Spring Security OAuth2.0认证授权一：框架搭建和认证测试](https://blog.kdyzm.cn/post/24)
> 
> [Spring Security OAuth2.0认证授权二：搭建资源服务](https://blog.kdyzm.cn/post/25)
> 
> [Spring Security OAuth2.0认证授权三：使用JWT令牌](https://blog.kdyzm.cn/post/26)
> 
> [Spring Security OAuth2.0认证授权四：分布式系统认证授权](https://blog.kdyzm.cn/post/30)
>
> [Spring Security OAuth2.0认证授权五：用户信息扩展到jwt](https://blog.kdyzm.cn/post/31)
>
> [Spring Security OAuth2.0认证授权六：前后端分离下的登录授权](https://blog.kdyzm.cn/post/38)
## 项目介绍
### 1. 模块介绍
| 模块名 | 模块介绍 |
| --- | ---|
| [auth-server](auth-server) | 认证服务 |
| [resource-server](resource-server)| 资源服务|
| [register-server](register-server)| 注册中心服务|
| [gateway-server](gateway-server)| 网关服务 |
| [business-server](business-server) | 前端页面web容器服务 |
| [common](common) |公共模块，保存公共功能|

### 2.服务启动顺序
项目启动前务必先执行 [auth-server/docs/sql/init.sql](auth-server/docs/sql/init.sql) 脚本。

项目启动顺序：
1. register-server
2. gateway-server
3. auth-server
4. resource-server
5. business-server

### 3.项目测试方法
启动成功后打开浏览器，输入`http://127.0.0.1:30002/`地址，就会看到以下页面    
![2021-01-29_171442.jpg](git-imgs/2021-01-29_171442.jpg)  
点击登录之后，出现登录框  
![2021-01-29_171532.jpg](git-imgs/2021-01-29_171532.jpg)  
输入账号密码之后，登录成功之后会跳转首页，就会看到个人信息  
![2021-01-29_171754.jpg](git-imgs/2021-01-29_171754.jpg)  
这里设置的token有效期为10秒，所以很快token就会失效，十秒钟之后刷新页面就会有新的提示  
![2021-01-29_171858.jpg](git-imgs/2021-01-29_171858.jpg)  
接下来可以有两种选择，一种是使用refresh-token更新失效的令牌，另外一种是重新登录，这里refresh_token的有效期也很短，只有30秒，如果超出30秒，则会更新失败，提示如下  
![2021-01-29_172049.jpg](git-imgs/2021-01-29_172049.jpg)  
而如果在30秒内刷新令牌，则会重新获取到令牌并刷新当前页  



